-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2022 at 06:31 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobtes`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(50) UNSIGNED NOT NULL,
  `nama` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Watson\'s Stores Rdn Bhd', '2022-04-04 03:32:55', '2022-04-04 03:32:55');

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `id` int(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `location_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `salary` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`id`, `title`, `location_id`, `company_id`, `salary`, `time`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Sales Manager', 1, 1, 'RM4,000 - RM6,000', 'Full Time', 'Candidcates must at least prosess Diploma level | Required language(s). Bahasa Malaysia, English | Love meeting with new people and learning new things | A quick thinker and generous | Full time and part time...', '2022-04-04 03:31:19', '2022-04-04 03:31:19'),
(2, 'Brand Ambassador', 2, 1, 'RM4,000 - RM6,000', 'Full Time', 'Candidcates must at least prosess Diploma level | Required language(s). Bahasa Malaysia, English | Love meeting with new people and learning new things | A quick thinker and generous | Full time and part time...', '2022-04-04 03:32:06', '2022-04-04 03:32:06'),
(3, 'Store Clerk', 3, 1, 'RM2,000 - RM4,000', 'Part Timrr', 'Candidcates must at least prosess Diploma level | Required language(s). Bahasa Malaysia, English | Love meeting with new people and learning new things | A quick thinker and generous | Full time and part time...', '2022-04-04 03:32:40', '2022-04-04 03:32:40');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(20) UNSIGNED NOT NULL,
  `nama` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Kota Kinabalu', '2022-04-04 03:33:25', '2022-04-04 03:33:25'),
(2, 'Selangor', '2022-04-04 03:33:25', '2022-04-04 03:33:25'),
(3, 'Kuala Lumpur', '2022-04-04 03:33:35', '2022-04-04 03:33:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(50) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
