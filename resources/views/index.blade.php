@include('layout.header')

<div class="container" style="width: 824px;padding-bottom: 50px;padding-top: 60px;" id="jobboard">
        <!-- Search Form -->
                <div class="main-nav2">
                    <div class="container">
                        <div class="row" style="margin: 20px;">
                            <div class="col-lg-3 col-md-6">
                          <input v-model="search" class="form-control" type="" placeholder="job title, company" name="title" autocomplete="off" style="border-radius:20px;">
                            </div>

                            <div class="col-lg-3 col-md-6 location">
                           <input v-model="searchLocation" type="" class="form-control" placeholder="Location" name="location" autocomplete="off" style="border-radius:20px;">
                            </div>

                            <div class="col-lg-3 col-md-6 salary">
                                <select class="form-control" placeholder="Min Salary (IDR)" name="min_salary" style="border-radius:20px;font-size: 12px;color: grey;">
                                  <option value="rm1">RM4000 - RM6000</option>
                                  <option value="rm1">RM10000 - RM16000</option>
                                </select>
                            </div>

                            <div class="col-lg-3 col-md-6 findjob">
                                <button class="default-btn" type="submit" style="font-size: 14px;"> <i class='search-btn bx bx-search'></i> Find Job </button>
                            </div>
                        </div>
                    </div>
                </div>
        <!-- End Search Form -->

    <!-- Board Job -->
    <div id="Job-Board" v-for="job in filteredJob" :key="job.id">
        <div class="pt-45 pb-45">
             <div class="container status-bg" style="border-radius:20px;">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="choose-content-list" style="display: flex;margin-top: 0px;">
                        <img src="assets/img/AJT/watsons-logo.png" alt="Logo Watson" style="width: 35%;">
                            <div class="content"  style="margin-left: 10px;margin-top: 30px;margin-bottom: 20px;">
                                <h1>@{{ job.title}}</h1>
                                <h2>@{{ job.company}}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6"></div>
                </div>

                 <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="row" style="display: inline-flex;font-size: 20px;margin-left: 45px;font-size: 15px;">
                            <i class='bx bx-money' style="border:none;color: black !important;font-size: 20px;"></i>
                               <h2 style="line-height: 2;">&nbsp;@{{ job.salary}}</h2><br>
                        </div>
                         <div class="row" style="display:flex;font-size: 20px;margin-left: 45px;font-size: 15px;">
                            <i class='bx bxs-map-pin' style="border:none;color: black !important;font-size: 20px;"></i>
                               <h2 style="line-height: 2;">&nbsp;@{{ job.location}}</h2><br>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                       <div class="row" style="display: inline-flex;font-size: 20px;margin-left: 45px;font-size: 15px;">
                             <i class='bx bx-timer' style="border:none;color: black !important;font-size: 20px;"></i>
                             <h2 style="line-height: 2;">&nbsp;@{{ job.time}}</h2><br>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <div class="row" style="display: inline-flex;margin-left: 45px;">
                            <h3>@{{ job.description}}</h3>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-6 col-sm-6" style="text-align: center;">
                        <div class="row" style="display: inline-flex;font-size: 20px;font-size: 15px;">
                            <i class='bx bx-heart' style="border:none;color: black !important;font-size: 20px;"></i>
                            <h2 style="line-height: 2;">&nbsp;Save</h2><br>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6" style="text-align: center;">
                       <div class="row" style="display: inline-flex;font-size: 20px;font-size: 15px;">
                             <i class='bx bx-briefcase' style="border:none;color: black !important;font-size: 20px;"></i>
                              <h2 style="line-height: 2;">&nbsp;Apply</h2><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Board Job End -->
</div>
 
@include('layout.footer')
<script type="text/javascript">
    new Vue({
  el: '#jobboard',
  data() {
    return {
      search: '',
      searchLocation: '',
      jobs:{!! $listjob !!},
    }
  },
   computed: {
    filteredJob(){
      
      return this.jobs.filter( (job) =>
  {
    return job.title.toLowerCase().includes(this.search) || job.company.toLowerCase().includes(this.search) 
  })

    }
  }
})

Vue.config.true = false
Vue.config.true = false
</script>