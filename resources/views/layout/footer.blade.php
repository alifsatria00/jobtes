        <!-- Footer -->
        <footer>
        <div class="copy-right-area">
            <div class="container">
                <div class="copy-right-text text-center">
                    <p>
                        Copyright @2022
                    </p>
                </div>
            </div>
        </div>
        </footer>
        <!-- End Footer -->

        <!-- Vue JS -->
        <script src="{{asset('assets/js/vue.js')}}"></script>
         <!-- Vue Resource JS -->
        <script src="{{asset('assets/js/vue-resource.js')}}"></script>
        <!-- Jquery Min JS -->
        <script src="{{asset('assets/js/jquery-3.5.1.slim.min.js')}}"></script>
        <!-- Popper Min JS -->
        <script src="{{asset('assets/js/popper.min.js')}}"></script>
        <!-- Bootstrap Min JS -->
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <!-- Owl Carousel Min JS -->
        <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
        <!-- Wow Min JS -->
        <script src="{{asset('assets/js/wow.min.js')}}"></script>
        <!-- Magnific Popup Min JS -->
        <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
        <!-- Meanmenu JS -->
        <script src="{{asset('assets/js/meanmenu.js')}}"></script>
        <!-- Ajaxchimp Min JS -->
        <script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>
        <!-- Form Validator Min JS -->
        <script src="{{asset('assets/js/form-validator.min.js')}}"></script>
        <!-- Contact Form JS -->
        <script src="{{asset('assets/js/contact-form-script.js')}}"></script>
        <!-- Custom JS -->
        <script src="{{asset('assets/js/custom.js')}}"></script>
       
    </body>
</html>