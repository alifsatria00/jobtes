 <style type="text/css">
     .form-control::placeholder {
  color: #6c757d;
  opacity: 1;
  font-size: 11px;
}
 </style>
  <!-- Start Navbar Area -->
        <div class="navbar-area">
            <!-- Menu For Mobile Device -->
            <div class="mobile-nav">
                <a href="/" class="logo">
                    <img src="assets/img/AJT/maukerja-logo.png" alt="Logo">
                </a>
                 <input v-model="search" class="form-control" type="" placeholder="Search Job" name="title" autocomplete="off">
            </div>

            <!-- Menu For Desktop Device -->
            <div class="main-nav">
                <div class="container">
                    <nav class="navbar navbar-expand-md navbar-light ">
                        <a class="navbar-brand" href="/">
                            <img src="assets/img/AJT/maukerja-logo.png" alt="Logo">
                        </a>

                        <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                            <ul class="navbar-nav m-left">
                                <li class="nav-item">
                                    <a href="#" class="nav-link active">
                                        All Jobs
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Tools 
                                        <i class='bx bx-chevron-down'></i>
                                    </a>
                                    <!--
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                
                                            </a>
                                        </li>
                                    </ul>
                                -->
                                </li>
                                    <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Help 
                                        <i class='bx bx-chevron-down'></i>
                                    </a>
                                    <!--
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                
                                            </a>
                                        </li>
                                    </ul>
                                -->
                                </li>

                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Blog 
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            </div>
        </div>
        <!-- End Navbar Area -->

