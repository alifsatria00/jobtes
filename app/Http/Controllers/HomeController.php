<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Jenssegers\Agent\Agent;
use App\Job;
use App\Location;
use App\Company;

class HomeController extends Controller
{

    public function index(Request $request)
    {

         $job = "SELECT j.id, j.title,l.nama as location,c.nama as company,j.salary,j.time,j.description
                    FROM job j
                    LEFT JOIN location l ON  j.location_id = l.id 
                    LEFT JOIN company c ON  c.id = j.company_id ";
        $listjob=DB::connection()->select($job);
    //dd($listjob);exit;
        $listjob=(json_encode($listjob));

          return view('index', compact('listjob'));
    }
}
