<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Job extends BaseModel
{
     public $table = "job";
    const CREATED_AT='created_at';
    const UPDATED_AT='updated_at';
    public function location() {
        return $this->belongsTo(Location::class);
    }

     public function company() {
        return $this->belongsTo(Company::class);
    }
}
